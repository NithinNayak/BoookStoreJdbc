package com.mindtree.daoimpl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.mindtree.utility.DbConnector;

public class DaoImpl {
	Connection con;//try doin null
	//Statement stmt;
	PreparedStatement pstmt;
	ResultSet res;
	String query;
	public ResultSet retrieveCategory() throws ClassNotFoundException, SQLException
	{
		if(con==null)
			con=DbConnector.dbConnection();
		try{
		query="SELECT DISTINCT CATEGORY FROM BOOKSTORE_DB.BOOK";
		pstmt=con.prepareStatement(query);
		res=pstmt.executeQuery();
		return res;}
		catch(SQLException e)
		{
			throw new SQLException("1");
		}
	}
	public ResultSet retrieveBookDetails(String category) throws SQLException, ClassNotFoundException
	{
		if(con==null)
			con=DbConnector.dbConnection();
		try{
		query="SELECT BOOK_ID,BOOK_NAME,AUTHOR_NAME,PUBLISHER,PRICE FROM BOOKSTORE_DB.BOOK WHERE CATEGORY=?";
		pstmt=con.prepareStatement(query);
		pstmt.setString(1, category);
		res=pstmt.executeQuery();
		return res;}
		catch(SQLException e)
		{
			throw new SQLException("2");
		}
	}
	public ResultSet retrieveBookId() throws ClassNotFoundException, SQLException
	{
		if(con==null)
			con=DbConnector.dbConnection();
		try{
		query="SELECT BOOK_ID FROM BOOKSTORE_DB.BOOK";
		pstmt=con.prepareStatement(query);
		res=pstmt.executeQuery();
		return res;
		}
		catch(SQLException e)
		{
			throw new SQLException("3");
		}
	}
	public ResultSet retrivPurchseNo() throws ClassNotFoundException, SQLException
	{
		if(con==null)
			con=DbConnector.dbConnection();
		try{
		query="SELECT MAX(PURCHASE_NO) FROM BOOKSTORE_DB.PURCHASE";
		pstmt=con.prepareStatement(query);
		res=pstmt.executeQuery();
		return res;}
		catch(SQLException e)
		{
			throw new SQLException("4");
		}
	}
	public ResultSet retrvBookPrice(int bookId) throws ClassNotFoundException, SQLException
	{
		if(con==null)
			con=DbConnector.dbConnection();
		try{
		query="SELECT PRICE FROM BOOKSTORE_DB.BOOK WHERE BOOK_ID=?";
		pstmt=con.prepareStatement(query);
		pstmt.setInt(1, bookId);
		res=pstmt.executeQuery();
		return res;}
		catch(SQLException e)
		{
			throw new SQLException("5");
		}
	}
	public void insertIntoTble(int bookId,String customerName,String customerMobileNo) throws ClassNotFoundException, SQLException
	{
		if(con==null)
			con=DbConnector.dbConnection();
		try{
		query="INSERT INTO BOOKSTORE_DB.PURCHASE(BOOK_ID,CUSTOMER_NAME,CUSTOMER_MOBILENO,PURCHASE_DATE) VALUES(?,?,?,CURDATE())";
		pstmt=con.prepareStatement(query);
		pstmt.setInt(1, bookId);
		pstmt.setString(2, customerName);
		pstmt.setString(3, customerMobileNo);
		pstmt.executeUpdate();}
		catch(SQLException e)
		{
			throw new SQLException("6");
		}
	}
	public void updtTble(int price,int bookId) throws ClassNotFoundException, SQLException
	{
		if(con==null)
			con=DbConnector.dbConnection();
		try{
		query="UPDATE BOOKSTORE_DB.PURCHASE SET AMOUNT=? WHERE BOOK_ID=?";
		pstmt=con.prepareStatement(query);
		pstmt.setInt(1, price);
		pstmt.setInt(2, bookId);
		pstmt.executeUpdate();}
		catch(SQLException e)
		{
			throw new SQLException("7");
		}
	}
	
}
